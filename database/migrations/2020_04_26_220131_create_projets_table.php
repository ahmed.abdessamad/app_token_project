<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('Nom');
            $table->string('Passport')->unique();
            $table->string('Pays');
            $table->integer('Code_Postal');
            $table->string('Equipe');
            $table->string('Domaine');
            $table->string('Business_Plan');
            $table->float('Montant_total');
            $table->float('Montant_Depart');
            $table->timestamps();
            $table->integer('User_id')->unsigned();
            $table->foreign('User_id')->references('id')->on('Users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projets');
    }
}
