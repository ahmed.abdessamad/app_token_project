<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InvestisseurProjet extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investisseurs_projets', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->integer('Investisseur_id')->unsigned();
            $table->foreign('Investisseur_id')->references('id')->on('Projets');

            $table->integer('Projet_id')->unsigned();
            $table->foreign('Projet_id')->references('id')->on('Investisseurs');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
