<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Projet extends Model
{
    public  $table = "projets";

    public  function user(){
        return $this->belongsTo('App\User');

    }




    public  function investisseurs()
    {
        return $this->belongsToMany('App\Investisseur');
    }
}
