<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Investisseur extends Model
{
    public  $table ="investisseurs";


    public  function projets()
    {
    return $this->belongsToMany('App\Projet');
    }
}
